/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayDeque;
import java.util.ArrayList;


/**
 *
 * @author Christian
 * This is the Chef class: it has the responsibility of preparing 
 * (processing)orders after an order is processed, it is removed from the 
 * working queue of open orders
 */
public class Chef {
    ArrayDeque<Order> orderQueue;
    /**
     * Constructor
     */
    public Chef() {
        orderQueue = new ArrayDeque<>();
    }
    
    /**
     * Adds an order to his order list
     * @param o order to add to list
     */
    public void acceptOrder(Order o) {
        orderQueue.add(o);        
    }
    
    /**
     * 
     * @return ArrayList version of his order queue
     */
    public ArrayList<Order> getList(){
        return new ArrayList(orderQueue);
    }
    
    /**
     * Processes an order.
     * Pre: 0 or more orders are waiting to be "cooked"
     * Post: oldest order is cooked, and removed from list
     * @param inv
     * @param srto 
     */
    public void processOrder(Inventory inv, SpecialRequestTO srto) {
        if (orderQueue.isEmpty()) return;
        Order orderInProcess = orderQueue.pop();
        for (Food f : orderInProcess.getFoodInOrder()) {
            for (Ingredient i : f.getIngredients().getIngredients()) { 
                inv.removeOne(i);
            }
        }
        if (orderInProcess.getRequests() != null) {
            srto.addRequest(orderInProcess.getRequests());
        }
    }
    
}
