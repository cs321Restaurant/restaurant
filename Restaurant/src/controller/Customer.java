/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;

/**
 *
 * @author Christian
 * 
 * This is the customer class. Represents a customer, making an order
 */
public class Customer {
    private ArrayList<Food> cart;

    /**
     * Constructor
     */
    public Customer() {
        cart = new ArrayList<>();
    }
    /**
     * adds a food item to cart
     * @param f Food to be added to cart
     */
    public void addToCart(Food f){
        cart.add(f);
    }
    /**
     * removes a food item from cart
     * @param f Food to be removed
     */
    public void removeFromCart(Food f){
        cart.remove(f);
    }
    /**
     * Clears out customer's cart
     */
    public void emptyCart(){
        cart.clear();
    }
    /**
     * Completes customer order
     * @return Order - customers complete order
     */
    public Order checkout() {
        Order order = new Order(cart);
        return order;
    }
}
