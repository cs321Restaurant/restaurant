/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

/**
 *
 * @author Christian
 * 
 * Food class, represents food items on the menu and in an order
 */
public class Food {
    /** 
     * Constructor
     * @param ingredients
     * @param name
     * @param price
     * @param type 
     */
    public Food(IngredientList ingredients, String name, int price, String type) {
        this.ingredients = ingredients;
        this.name = name;
        this.price = price;
        this.type = type;
        this.cost = ingredients.getCost();
    }
    
    /**
     * get the ingredients that comprise a food item
     * @return IngredientList
     */
    public IngredientList getIngredients(){
        return ingredients;
    }
    /**
     * get name of food
     * @return 
     */
    public String getName() {
        return name;
    }
    
    /**
     *Get the price of an item
     * @return int, price of item
     */
    public int getPrice() {
        return price;
    }
    
    /**
     *Sets a new price of the item
     * @param newPrice
     */
    public void setPrice(int newPrice) {
        this.price = newPrice;
    }

    /**
     * get the type of an item (Sandwich, side, dessert, etc.)
     * @return
     */
    public String getType() {
        return type;
    }

    /**
     * Set a food item's type
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }
    
    private IngredientList ingredients;
    private String name;
    private int price;
    private int cost;
    private String type;
}
