/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.Objects;

/**
 *
 * @author Christian
 * 
 * Ingredient class represents ingredients of a food item, and in the inventory
 */
public class Ingredient {

    /**
     * Constructor
     * @param name
     */
    public Ingredient(String name) {
        this.name = name.toLowerCase();
    }

    /**
     * Constructor
     * @param name
     * @param cost
     */
    public Ingredient(String name, int cost) {
        this.name = name;
        this.cost = cost;
    }

    /**
     * get the name of an ingredient
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * get the cost of an ingredient
     * @return
     */
    public int getCost() {
        return cost;
    }
    
    /**
     * set the cost of an ingredient 
     * @param newCost
     */
    public void setCost(int newCost) {
        this.cost = newCost;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Ingredient other = (Ingredient) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }
    
    
    private final String name;
    private int cost;
}
