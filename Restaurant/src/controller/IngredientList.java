/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;


/**
 *
 * @author Christian
 * 
 * a list of ingredients
 */
import java.util.ArrayList;

/**
 *
 * @author Christian
 */
public class IngredientList {
    private ArrayList<Ingredient> ingredients;

    /**
     * Constructor
     */
    public IngredientList() {
        ingredients = new ArrayList<>();
    }
    
    /**
     * add ingredient to the list
     * @param i
     */
    public void addIngredient(Ingredient i) {
        ingredients.add(i);
    }

    /**
     * get how many ingredients are in the list
     * @return
     */
    public int getNumberIngredients() {
        return ingredients.size();
    }

    /**
     * get all ingredients
     * @return the list of ingredients
     */
    public ArrayList<Ingredient> getIngredients() {
        return ingredients;
    }
    
    /**
     * get cost of all ingredients in the list
     * @return summed cost of all ingredients
     */
    public int getCost() {
        int totalCost = 0;
        for (Ingredient i : ingredients) {
            totalCost += i.getCost();
        }
        return totalCost;
    }
    
   
    
}
