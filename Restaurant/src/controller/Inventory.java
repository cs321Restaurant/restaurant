/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Christian
 * 
 * Inventory class represents the inventory, where all 
 * ingredients are held
 */
public class Inventory {
    
    /**
     * Constructor
     */
    public Inventory() {
        this.inventory = new HashMap<>();
    }
    
    /**
     * get quantity of an item in inventory
     * @param i Ingredient to check inventory level
     * @return int, how many there are
     */
    public int checkInventory(Ingredient i) {
        int invLevel = 0;
        try {
            invLevel = inventory.get(i);
        }
        catch(Exception e) {
            //dont need to do anything here, will only get here
            //if we had 0 of an item, and since the item was not found, 
            //will return 0 as expected
        }
        return invLevel;
    }

    /**
     * Sets an inventory amount for an item
     * overwrites any currently written amount
     * @param i ingredient to set amount of
     * @param n quantity of ingredient
     */
    public void setInventory(Ingredient i, int n) {
        inventory.put(i, n);
    }

    /**
     * add to current inventory level 
     * @param i ingredient to add to
     * @param n quantity to add
     */
    public void addToInventory(Ingredient i, int n) {
        Integer temp = checkInventory(i);
        inventory.put(i, temp + n);
    }
    
    /**
     * remove one item from inventory
     * @param i ingredient to remove
     */
    public void removeOne(Ingredient i) {
        Integer temp = checkInventory(i);
        inventory.put(i, temp - 1);
    }


    /**
     * remove many of an item from inventory
     * @param i ingredient to remove
     * @param n quantity to remove
     */
    public void removeMany(Ingredient i, int n) {
        Integer temp = checkInventory(i);
        inventory.put(i, temp - n);
    }
    
    /**
     * get an ingredient 
     * @param nameOfIngredient
     * @return Ingredient
     */
    public Ingredient getIngredient(String nameOfIngredient) {
        for (Ingredient i : inventory.keySet()) {
            if (nameOfIngredient.equalsIgnoreCase(i.getName())) {
                return i;
            }
        }
        return null;
    }
    
    /**
     * Gets the name of all ingredients
     * @return ArrayList of ingredient names 
     */
    public ArrayList<String> getAllIngredientNames() {
        ArrayList<String> ingredientNames = new ArrayList<>();
        for (Ingredient i : inventory.keySet()) {
            ingredientNames.add(i.getName());
        }
        return ingredientNames;
    }
    
    
    private final HashMap<Ingredient,Integer> inventory;
}
