/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;

/**
 *
 * @author Christian
 * 
 * Manager class to alter the inventory
 */
public class Manager {
    private Inventory inventory;
    private Menu menu;

    /**
     * Constructor
     * @param i
     * @param m
     */
    public Manager(Inventory i, Menu m) {
        this.inventory = i;
        this.menu = m;
    }
    
    /**
     * gets all ingredient names
     * @return list of the names of all ingredients 
     */
    public ArrayList<String> getAllIngredientNames() {
        return inventory.getAllIngredientNames();
    }
    
    /**
     * sets the inventory quantity
     * @param ingredientName
     * @param quantity
     */
    public void setInventoryQuantity(String ingredientName, int quantity) {
        inventory.setInventory(inventory.getIngredient(ingredientName), 
                quantity);
    }
    
    /**
     * check inventory levels
     * @param ingredientName
     * @return
     */
    public int checkInventory(String ingredientName) {
        return inventory.checkInventory(
                inventory.getIngredient(ingredientName));
    }
    
    /**
     * add item to menu
     * @param f
     */
    public void addItemToMenu(Food f) {
        menu.addToMenu(f);
    }
    
    /**
     * gets the cost of an ingredient 
     * @param ingredientName
     * @return
     */
    public int getInventoryItemCost(String ingredientName) {
        return inventory.getIngredient(ingredientName).getCost();
    }
    
}

