/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;

/**
 *
 * @author Christian
 * 
 * represents a menu at the restaurant
 */
public class Menu {
    private final ArrayList<Food> menuItems;

    /**
     * Constructor
     */
    public Menu() {
        menuItems = new ArrayList<>();
    }
    
    /**
     * add item to the menu
     * @param f Food item to add to the menu
     */
    public void addToMenu(Food f) {
        menuItems.add(f);
    }
    
    /**
     *  get an item from the menu
     * @param nameOfFood
     * @return Food item from menu
     */
    public Food getItem(String nameOfFood) {
        for (Food f : menuItems) {
            if (nameOfFood.equalsIgnoreCase(f.getName())) {
                return f;
            }
        }
        return null;
    }
    
    /**
     * get the names of all food items on the menu
     * @return Arraylist of menu item names
     */
    public ArrayList<String> getAllFoodNames() {
        ArrayList<String> menuNames = new ArrayList<>();
        for (Food f : menuItems) {
            menuNames.add(f.getName());
        }
        return menuNames;
    }
}
