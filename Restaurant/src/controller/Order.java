/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;

/**
 *
 * @author Christian
 * 
 * Represents an order, consisting of menu items. 
 */
public class Order {
    private ArrayList<Food> foodInOrder;
    private String specialRequests;

    /**
     * Constructor
     */
    public Order() {
        foodInOrder = new ArrayList<>();
    }
    
    /**
     *  Constructor
     * @param foodList
     */
    public Order(ArrayList<Food> foodList) {
        this.foodInOrder = foodList;
    }
    
    /**
     * get all food items in order
     * @return ArrayList of food items
     */
    public ArrayList<Food> getFoodInOrder() {
        return this.foodInOrder;
    }

    /**
     * set special requests
     * @param s
     */
    public void setRequests (String s){
        specialRequests = s;
    }

    /**
     * get special requests
     * @return
     */
    public String getRequests (){
        return specialRequests;
    }
}
