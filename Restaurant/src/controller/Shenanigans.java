/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.Scanner;
import view.Screen;
import readers.*;
import writers.*;

/**
 *
 * @author Christian
 */
public class Shenanigans {

    /**
     * driver class for program
     * @param args
     */
    public static void main(String[] args) { 
       
        MenuFileReader menuReader = new MenuFileReader();
        InventoryFileReader invReader = new InventoryFileReader();
        
        Menu menu = menuReader.Read();
        Inventory inventory = invReader.Read();
        Chef cookie = new Chef();
        Manager manager = new Manager(inventory, menu);
               
        Screen s = new Screen (menu, cookie, inventory, manager);
       

    }
}
