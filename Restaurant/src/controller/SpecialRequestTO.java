/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;

/**
 *
 * @author christian
 * 
 * Special request Transfer Object
 * used to transfer special requests from the chef to the manager
 */
public class SpecialRequestTO {
    private ArrayList<String> specialRequestList;

    /**
     * Constructor
     */
    public SpecialRequestTO() {
        this.specialRequestList = new ArrayList<>();
    }
    
    /**
     * returns list of special requests, if any. 
     * @return
     */
    public ArrayList<String> getSpecialRequestList() {
        return specialRequestList;
    }

     
    /**
     * add a request to the list
     * @param s
     */
    public void addRequest(String s) {
        specialRequestList.add(s);
    }
}
