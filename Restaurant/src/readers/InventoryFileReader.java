/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package readers;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import controller.*;
/**
 *
 * @author Ben
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ben
 */
public class InventoryFileReader implements InventoryReader{
    
    @Override
    public Inventory Read(){
        Inventory inv = new Inventory();
        String fileName =  "InventoryStorage.txt";
        String nextLine = null;
        BufferedReader bufferedInventoryFile = null;
        try{
            bufferedInventoryFile =
                new BufferedReader(
                    new FileReader(fileName));
            
            while((nextLine = bufferedInventoryFile.readLine()) != null){
                if(!nextLine.equals("")){
                    String ingredientName = nextLine.substring(
                            0, nextLine.indexOf("$"));
                            
                    int ingredientPrice = Integer.parseInt(
                            nextLine.substring(
                                nextLine.indexOf("$")+1, nextLine.indexOf("#"))

                    );
                    
                    Ingredient nextIngr = new Ingredient(
                            ingredientName, ingredientPrice);
                    
                    int numberOfIngr = Integer.parseInt(
                            nextLine.substring(
                                nextLine.indexOf("#")+1));
                    
                   inv.setInventory(nextIngr, numberOfIngr);
                    
                    


                }
            }
            bufferedInventoryFile.close();
        }
        catch(IOException e){
           inv = null;
        }
        return inv;
    }
        
    
}
