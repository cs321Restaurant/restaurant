/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package readers;

import controller.Inventory;

/**
 *
 * @author christian
 * 
 * Interface for reading and instantiating an inventory object from disc
 */
public interface InventoryReader{
    public Inventory Read();
}
