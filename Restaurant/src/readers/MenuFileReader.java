/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package readers;
import java.io.*;
import controller.*;
import java.util.ArrayList;

/**
 *
 * @author Ben
 */
  

public class MenuFileReader implements MenuReader{
    Menu restrauntMenu;
    public MenuFileReader(){
      //restrauntMenu = FileReader();  
    }
   /**
    
    @return
    * */
    @Override
    public Menu Read() {
        Menu menu = new Menu();
        String fileName = "MenuStorage.txt";
        String nextLine = null;
        BufferedReader bufferedMenuFile = null;
        try{
            bufferedMenuFile =
                new BufferedReader(
                    new FileReader(fileName));
            
            while((nextLine = bufferedMenuFile.readLine()) != null){
                if(!nextLine.equals("")){
                    String itemName = nextLine.substring(0,nextLine.indexOf(":"));
                    IngredientList theIngredients = new IngredientList();
                    //ArrayList<String> ingredientList = new ArrayList<String> ();
                    String ingredients[] = nextLine.substring(
                            nextLine.indexOf(":")+1,
                            nextLine.indexOf("$") )
                            .split(";");

                    for(String a : ingredients){
                        theIngredients.addIngredient(new Ingredient(a));
                    }

                    int itemPrice = Integer.parseInt(
                            nextLine.substring(
                                nextLine.indexOf("$")+1, nextLine.indexOf("#"))

                    );
                    
                    String itemCategory = nextLine.substring(
                            nextLine.indexOf("#")+1);
                    
                    
                    menu.addToMenu(new Food(
                            theIngredients, itemName, itemPrice, itemCategory)
                    );
                    


                }
            }
            bufferedMenuFile.close();
        }
        catch(Exception e){
            menu = null;
        }
        return menu;
    }
    
    
    
}

