/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package readers;

import controller.Menu;

/**
 *
 * @author christian
 * Interface for reading and instantiating a menu object from disc
 */
public interface MenuReader {
    Menu Read();
}

