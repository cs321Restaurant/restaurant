/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package writers;
import controller.*;
import java.io.*;
import java.util.ArrayList;



/**
 *
 * @author Ben
 */
public class InventoryFileWriter implements InventoryWriter{
    
    @Override
    public void Write(Manager manager){
        try{
            BufferedWriter a = new BufferedWriter(
                    new FileWriter("InventoryStorage.txt"));
            ArrayList<String> Ingredients= manager.getAllIngredientNames();
            String line;
            for(String ingredient: Ingredients){
                line = ingredient + "$" + manager.getInventoryItemCost(ingredient) +
                        "#" + manager.checkInventory(ingredient);
                a.write(line);
                a.newLine();
            }
            
            
            a.close();
        }
        catch(Exception e){
            System.out.println("Warning, Error in saving!");
        }
        
    }
}
