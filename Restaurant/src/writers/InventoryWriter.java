/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package writers;


import controller.Manager;

/**
 *
 * @author christian
 * interface for writing an inventory object to disc
 */
public interface InventoryWriter{
    public void Write(Manager manager);
}
