/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package writers;
import controller.*;
import java.io.*;
import java.util.ArrayList;


/**
 *
 * @author Ben
 */
public class MenuFileWriter implements MenuWriter{
    
    @Override
    public void Write(Menu output){
        try{
            BufferedWriter a = new BufferedWriter(
                    new FileWriter("MenuStorage.txt"));
            //not implemented yet, needs more Menu methods
            ArrayList<String> foodNames = output.getAllFoodNames();
            String line;
            String ingredients = "";
            for(String food: foodNames){
                Food thisOne = output.getItem(food);
                for(Ingredient ingredient : thisOne.getIngredients().getIngredients()){
                    ingredients += ingredient.getName() + ";";
                }
                ingredients = ingredients.substring(0, ingredients.length()-1);
                line = (food + ":" + ingredients + "$" + 
                        thisOne.getPrice() + "#" + thisOne.getType());
                a.write(line);
                a.newLine();
                ingredients = "";
            }
            
            
            a.close();
        }
        catch(Exception e){
            System.out.println("Warning, Error in saving!");
        }
        
    }
}
