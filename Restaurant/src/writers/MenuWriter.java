/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package writers;

import controller.Menu;

/**
 *
 * @author christian
 * interface for writing a menu object to disc
 */
public interface MenuWriter{
    public void Write(Menu output);
}
