/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Christian
 */
public class InventoryTest {
    
    Ingredient testIngredient;
    
    public InventoryTest() {       
    }
 
    
    @Before
    public void setUp() {
        testIngredient = new Ingredient("Cheese", 5);
    }


    /**
     * Test of checkInventory method, of class Inventory.
     */
    @Test
    public void testCheckInventory() {
        System.out.println("checkInventory");
        Ingredient i = testIngredient;
        Inventory instance = new Inventory();
        int expResult = 0;
        int result = instance.checkInventory(i);
        assertEquals(expResult, result);
        
        int expResult2 = 10;
        instance.addToInventory(testIngredient, expResult2 );
        result = instance.checkInventory(i);
        assertEquals(expResult2, result);
    }

    /**
     * Test of setInventory method, of class Inventory.
     */
    @Test
    public void testSetInventory() {
        System.out.println("setInventory");
        Ingredient i = testIngredient;
        int n = 10;
        Inventory instance = new Inventory();
        instance.setInventory(i, n);
        assertEquals(instance.checkInventory(i),n);

    }

    /**
     * Test of addToInventory method, of class Inventory.
     */
    @Test
    public void testAddToInventory() {
        System.out.println("addToInventory");
        Ingredient i = testIngredient;
        int n = 10;
        Inventory instance = new Inventory();
        instance.addToInventory(i, n);
        assertEquals(instance.checkInventory(i),n);

    }

    /**
     * Test of removeOne method, of class Inventory.
     */
    @Test
    public void testRemoveOne() {
        System.out.println("removeOne");
        Ingredient i = testIngredient;
        int n = 10;
        Inventory instance = new Inventory();
        instance.addToInventory(i, n);
        instance.removeOne(i);
        assertEquals(instance.checkInventory(i),n-1);

    }

    /**
     * Test of removeMany method, of class Inventory.
     */
    @Test
    public void testRemoveMany() {
        System.out.println("removeMany");
        Ingredient i = testIngredient;
        int n = 10;
        int remove = 5;
        Inventory instance = new Inventory();
        instance.addToInventory(i, n);
        instance.removeMany(i, remove);
        assertEquals(instance.checkInventory(i),n-remove);

    }

    /**
     * Test of getIngredient method, of class Inventory.
     */
    @Test
    public void testGetIngredient() {
        System.out.println("getIngredient");
        String nameOfIngredient = "Cheese";
        Inventory instance = new Inventory();
        instance.addToInventory(testIngredient, 10);
        Ingredient expResult = testIngredient;
        Ingredient result = instance.getIngredient(nameOfIngredient);
        assertEquals(expResult, result);

    }

    /**
     * Test of getAllIngredientNames method, of class Inventory.
     */
    @Test
    public void testGetAllIngredientNames() {
        System.out.println("getAllIngredientNames");
        Inventory instance = new Inventory();
        instance.addToInventory(testIngredient, 1);
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("Cheese");
        ArrayList<String> result = instance.getAllIngredientNames();
        assertEquals(expResult, result);

    }
    
}
